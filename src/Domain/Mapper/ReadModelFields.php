<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Mapper;

class ReadModelFields
{
    const ID = 'id';
    const METADATA = 'metadata';
    const NAME = 'name';
    const PROJECTION = 'projection';
    const EVENT_ID = 'event_id';
}