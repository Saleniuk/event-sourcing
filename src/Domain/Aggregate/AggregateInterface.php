<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Aggregate;

/**
 * Interface AggregateInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Aggregate
 */
interface AggregateInterface
{
    /**
     * @return array
     */
    public function toArray(): array;
}