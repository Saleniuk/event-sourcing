<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Aggregate;

use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Interface EventAggregateInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Aggregate
 */
interface EventAggregateInterface
{
    /**
     * @param EventInterface $event
     * @return mixed
     */
    public function addEvent(EventInterface $event);

    /**
     * @return array
     */
    public function getEvents(): array;
}