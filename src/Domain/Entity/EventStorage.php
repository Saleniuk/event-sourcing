<?php

namespace JakubSaleniuk\EventSourcing\Domain\Entity;

/**
 * Class EventStorage
 * @package JakubSaleniuk\EventSourcing\Domain\Entity
 */
class EventStorage
{
    /** @var int */
    private $id;

    /** @var string */
    private $uuid;

    /** @var string */
    private $name;

    /** @var string */
    private $aggregateId;

    /** @var array */
    private $payload;

    /**
     * EventStorage constructor.
     * @param int $id
     * @param string $uuid
     * @param string $name
     * @param string $aggregateId
     * @param array $payload
     */
    public function __construct($id, $uuid, $name, $aggregateId, array $payload)
    {
        $this->id = $id;
        $this->uuid = $uuid;
        $this->name = $name;
        $this->aggregateId = $aggregateId;
        $this->payload = $payload;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }
}