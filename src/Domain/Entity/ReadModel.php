<?php

namespace JakubSaleniuk\EventSourcing\Domain\Entity;

/**
 * Class ReadModel
 * @package JakubSaleniuk\EventSourcing\Domain\Entity
 */
class ReadModel
{
    /** @var int */
    private $id;

    /** @var array */
    private $metadata;

    /** @var string */
    private $name;

    /** @var array */
    private $projection;

    /** @var int */
    private $eventId;

    /**
     * ReadModel constructor.
     * @param $id
     * @param array $metadata
     * @param $name
     * @param array $projection
     * @param int $eventId
     */
    public function __construct($id, array $metadata, $name, array $projection, int $eventId)
    {
        $this->id = $id;
        $this->metadata = $metadata;
        $this->name = $name;
        $this->projection = $projection;
        $this->eventId = $eventId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getProjection(): array
    {
        return $this->projection;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }
}