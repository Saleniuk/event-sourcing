<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Repository;

use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;

interface EventStorageRepositoryInterface
{
    public function getByAggregateId(string $aggregateId): array;

    public function save(EventStorage $eventStorage);

    public function getByEventNameAndMetadata(string $eventName, array $metadata): array;
}