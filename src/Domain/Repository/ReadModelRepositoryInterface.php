<?php

namespace JakubSaleniuk\EventSourcing\Domain\Repository;

use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;

/**
 * Interface ReadModelRepositoryInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Repository
 */
interface ReadModelRepositoryInterface
{
    /**
     * @param string $name
     * @param array $conditions
     * @return array
     */
    public function find(string $name, array $conditions): array;

    /**
     * @param ReadModel $readModel
     * @return mixed
     */
    public function save(ReadModel $readModel);

    /**
     * @param string $name
     * @param string $aggregateId
     * @param int $eventId
     * @return void
     */
    public function remove(string $name, string $aggregateId, int $eventId);
}