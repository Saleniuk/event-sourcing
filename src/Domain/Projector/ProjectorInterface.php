<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Aggregate;

use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Interface ProjectorInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Aggregate
 */
interface ProjectorInterface
{
    /**
     * @param EventInterface $event
     * @return AggregateInterface
     */
    public function applyEvent(EventInterface $event): AggregateInterface;

    /**
     * @param EventAggregateInterface $eventAggregate
     * @return AggregateInterface
     */
    public function applyEvents(EventAggregateInterface $eventAggregate): AggregateInterface;

    /**
     * @return string
     */
    public function getProjectionName(): string;
}