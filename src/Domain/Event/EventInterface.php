<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Event;

interface EventInterface
{
    public function getId(): int;

    public function setId(int $id);

    public function getAggregateId(): string;

    public function getEventName(): string;

    public function getCreatedAt(): string;

    public function toArray(): array;
}