<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Event;

/**
 * Class EventBusInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Event
 */
interface EventBusInterface
{
    /**
     * @param EventInterface $event
     * @return void
     */
    public function dispatch(EventInterface $event);
}