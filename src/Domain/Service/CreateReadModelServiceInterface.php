<?php

declare(strict_types = 1);

namespace JakubSaleniuk\EventSourcing\Domain\Service;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Interface CreateReadModelServiceInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Service
 */
interface CreateReadModelServiceInterface
{
    /**
     * @param EventInterface $event
     * @param AggregateInterface $aggregate
     * @return mixed
     */
    public function createReadModel(EventInterface $event, AggregateInterface $aggregate);
}