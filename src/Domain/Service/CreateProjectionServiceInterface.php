<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Service;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;

/**
 * Interface CreateProjectionServiceInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Service
 */
interface CreateProjectionServiceInterface
{
    /**
     * @param string $aggregateId
     * @return AggregateInterface
     */
    public function execute(string $aggregateId): AggregateInterface;
}