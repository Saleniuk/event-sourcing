<?php

declare(strict_types = 1);

namespace JakubSaleniuk\EventSourcing\Domain\Service;

use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Interface DispatchEventServiceInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Service
 */
interface DispatchEventServiceInterface
{
    /**
     * @param EventInterface $event
     * @return mixed
     */
    public function dispatch(EventInterface $event);
}