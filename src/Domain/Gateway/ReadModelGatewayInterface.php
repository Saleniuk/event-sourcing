<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Gateway;

/**
 * Interface ReadModelGatewayInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Gateway
 */
interface ReadModelGatewayInterface
{
    /**
     * @param string $name
     * @param array $condition
     * @return array
     */
    public function find(string $name, array $condition): array;

    /**
     * @param array $readModelData
     * @return mixed
     */
    public function save(array $readModelData);

    /**
     * @param string $name
     * @param string $aggregateId
     * @param int $eventId
     * @return void
     */
    public function remove(string $name, string $aggregateId, int $eventId);
}