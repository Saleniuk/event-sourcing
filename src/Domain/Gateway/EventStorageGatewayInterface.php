<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Gateway;

interface EventStorageGatewayInterface
{
    public function getByAggregateId(string $aggregateId): array;

    public function save(array $eventStorageData);

    public function getByEventNameAndMetadata(string $eventName, array $metadata): array;
}