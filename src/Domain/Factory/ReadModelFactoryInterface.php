<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Factory;

use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Interface ReadModelFactoryInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Factory
 */
interface ReadModelFactoryInterface
{
    /**
     * @param string $name
     * @param array $metadata
     * @param array $aggregate
     * @param EventInterface $event
     * @return ReadModel
     */
    public function create(string $name, array $metadata, array $aggregate, EventInterface $event): ReadModel;
}