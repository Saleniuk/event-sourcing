<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Factory;

use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;
use JakubSaleniuk\EventSourcing\Domain\Event\EventInterface;

/**
 * Interface EventStorageFactoryInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Factory
 */
interface EventStorageFactoryInterface
{
    /**
     * @param EventInterface $event
     * @return EventStorage
     */
    public function create(EventInterface $event): EventStorage;
}