<?php

declare(strict_types = 1);

namespace JakubSaleniuk\EventSourcing\Domain\Factory;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\EventAggregateInterface;

/**
 * Interface EventAggregateFactoryInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Factory
 */
interface EventAggregateFactoryInterface
{
    /**
     * @return EventAggregateInterface
     */
    public function create(): EventAggregateInterface;
}