<?php

declare(strict_types = 1);

namespace JakubSaleniuk\EventSourcing\Domain\Factory;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;

/**
 * Interface AggregateFactoryInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Factory
 */
interface AggregateFactoryInterface
{
    /**
     * @return AggregateInterface
     */
    public function createAggregate(): AggregateInterface;
}