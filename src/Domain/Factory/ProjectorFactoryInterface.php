<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Factory;

use JakubSaleniuk\EventSourcing\Domain\Aggregate\AggregateInterface;
use JakubSaleniuk\EventSourcing\Domain\Aggregate\ProjectorInterface;

/**
 * Interface ProjectorFactoryInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Factory
 */
interface ProjectorFactoryInterface
{
    /**
     * @param AggregateInterface $aggregate
     * @return ProjectorInterface
     */
    public function create(AggregateInterface $aggregate): ProjectorInterface;
}