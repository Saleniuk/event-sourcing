<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Hydrator;

use JakubSaleniuk\EventSourcing\Domain\Entity\ReadModel;

/**
 * Interface ReadModelHydratorInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Hydrator
 */
interface ReadModelHydratorInterface
{
    /**
     * @param array $readModelData
     * @return ReadModel
     */
    public function hydrate(array $readModelData): ReadModel;

    /**
     * @param ReadModel $readModel
     * @return array
     */
    public function extract(ReadModel $readModel): array;
}