<?php

declare(strict_types=1);

namespace JakubSaleniuk\EventSourcing\Domain\Hydrator;

use JakubSaleniuk\EventSourcing\Domain\Entity\EventStorage;

/**
 * Interface EventStorageHydratorInterface
 * @package JakubSaleniuk\EventSourcing\Domain\Hydrator
 */
interface EventStorageHydratorInterface
{
    /**
     * @param array $eventStorageData
     * @return EventStorage
     */
    public function hydrate(array $eventStorageData): EventStorage;

    /**
     * @param EventStorage $eventStorage
     * @return array
     */
    public function extract(EventStorage $eventStorage): array;
}