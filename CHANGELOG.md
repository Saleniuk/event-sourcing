# Changelog

## [0.2.23]

- Added a `getCreatedAt` method to EventInterface

## [0.2.19]

- Added a new method `getByEventNameAndMetadata` to EventStorageRepositoryInterface

## [0.2.3] - Jakub Saleniuk - 03.08.2017

- Added interface suffix to all interfaces names.

## [0.2.4] - Jakub Saleniuk - 03.08.2017

- Added toArray method in `AggregateInterface`

## [0.2.5] - Jakub Saleniuk - 04.08.2017

- Added `metadata` param to `createReadModel` method in 
CreateReadModelServiceInterface.

## [0.2.6] - Jakub Saleniuk - 04.08.2017

- Changed `create` method in ReadModelFactoryInterface.

## [0.2.7] - Jakub Saleniuk - 04.08.2017

- Added `getProjectionName` method to ProjectorInterface.
- Changed `Projector` name to `ProjectorInterface`.

## [0.2.8] - Jakub Saleniuk - 04.08.2017

- Changed all used `Projector` to `ProjectorInterface`.

## [0.2.9] - Jakub Saleniuk - 04.08.2017

- Changed `metadata` (array) param to `aggregate` (AggregateInterface) in 
createReadModel method in Create ReadModelServiceInterface.

## [0.2.10] - Jakub Saleniuk - 04.08.2017

- Added `getMetadata` method to `ProjectorInterface`.

## [0.2.11] - Jakub Saleniuk - 04.08.2017
    
- Removed `getMetadata` method from `ProjectorInterface`.

## [0.2.12] - Jakub Saleniuk - 04.08.2017

- Added CreateProjectionServiceInterface.

## [0.2.13] - Jakub Saleniuk - 04.08.2017

- Changed `create` method to 'execute' and added event (EventInterface)
param.

## [0.2.14] - Jakub Saleniuk - 04.08.2017

- Changed `create` method to `createAggregate` in `AggregateFactoryInterface`.

## [0.2.15] - Jakub Saleniuk - 07.08.2017

- Changed the `event` (EventInterface) parameterer to `aggregate_id` (string)
 in the execute method in the CreateProjectionServiceInterface.
 
## [0.2.16] - Jakub Saleniuk - 07.08.2017

- Removed `counter` and `metadata` fields in the EventStorage entity.

## [0.2.17] - Jakub Saleniuk - 07.08.2017

- Removed `counter` and `metadata` fields in the EventStorage entity.

## [0.2.18] - Jakub Saleniuk - 28.08.2017

- Changed `aggregate` param type in ReadModelFactoryInterface from AggregateInterface to array.